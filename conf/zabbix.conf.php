<?php
// Zabbix GUI configuration file.
global $DB, $HISTORY;

$DB['TYPE']				= 'MYSQL';
$DB['SERVER']			= 'mysql';
$DB['PORT']				= '0';
$DB['DATABASE']			= 'zabbix';
$DB['USER']				= 'zabbix';
$DB['PASSWORD']			= 'zabbix';
// Schema name. Used for IBM DB2 and PostgreSQL.
$DB['SCHEMA']			= '';

$ZBX_SERVER				= 'openshift-zabbix';
$ZBX_SERVER_PORT		= '10051';
$ZBX_SERVER_NAME		= 'openshift-zabbix';

$IMAGE_FORMAT_DEFAULT	= IMAGE_FORMAT_PNG;

// Elasticsearch url (can be string if same url is used for all types).
$HISTORY['url']   = [
		'uint' => 'http://localhost:9200',
		'text' => 'http://localhost:9200'
];
// Value types stored in Elasticsearch.
$HISTORY['types'] = ['uint', 'text'];
